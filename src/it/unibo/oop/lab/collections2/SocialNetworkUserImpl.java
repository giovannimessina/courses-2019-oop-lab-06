package it.unibo.oop.lab.collections2;

import java.util.*;
/**
 * 
 * Instructions
 * 
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 * 
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 * 
 * @param <U>
 *            Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

    /*
     * 
     * [FIELDS]
     * 
     * Define any necessary field
     * 
     * In order to save the people followed by a user organized in groups, adopt
     * a generic-type Map:
     * 
     * think of what type of keys and values would best suit the requirements
     */

    /*
     * [CONSTRUCTORS]
     * 
     * 1) Complete the definition of the constructor below, for building a user
     * participating in a social network, with 4 parameters, initializing:
     * 
     * - firstName - lastName - username - age and every other necessary field
     * 
     * 2) Define a further constructor where age is defaulted to -1
     */
	
	

    /**
     * Builds a new {@link SocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
	
	
	private final HashMap< String, Collection<U> > followedUser;
	
	public SocialNetworkUserImpl(final String name, final String surname, final String user) {
		this(name, surname, user, -1);
	}
	
    public SocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
        super(name, surname, user, userAge);
        followedUser = new HashMap<>();
    }

    /*
     * [METHODS]
     * 
     * Implements the methods below
     */

    public boolean addFollowedUser(final String circle, final U user) { 	
    	try {
    	if (!followedUser.containsValue( (Object)user )  && !this.getFollowedUsersInGroup(circle).isEmpty() ) {
    		
    		if(!followedUser.get(circle).contains(user)) {
    		followedUser.get(circle).add(user);
    		return true;
    		}
    	  	return false;
    	}else {
    		
    	List<U> list = new LinkedList<>();
    	list.add(user);
    	followedUser.put(circle,list);
    		return true;
    	}
    	
    	}catch(Exception e) {
    		System.out.println(e);
    	}
    	return false;
    
   }
   
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
    	 final Collection<U> friendsInCircle = followedUser.get(groupName);
    	 if(friendsInCircle != null) {
    	 return new ArrayList<>(friendsInCircle);
    	 }
    	 else {
    		 return new ArrayList<>();
    	 }
    }

    public List<U> getFollowedUsers() {
    
    	if(followedUser.isEmpty()) {
    	   return null;
       }
       
       List<U> ll = new LinkedList<>();
       Set<String> key = new TreeSet<>();
       key = followedUser.keySet();
       for(String o : key) {
    	   ll.addAll(this.getFollowedUsersInGroup(o));
       }
       return ll;
    }

}
